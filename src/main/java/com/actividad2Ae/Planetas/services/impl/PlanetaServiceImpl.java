package com.actividad2Ae.Planetas.services.impl;

import com.actividad2Ae.Planetas.data.dto.PlanetaDto;
import com.actividad2Ae.Planetas.data.entity.Planeta;
import com.actividad2Ae.Planetas.data.mapper.PlanetaMapper;
import com.actividad2Ae.Planetas.data.payload.PlanetaForm;
import com.actividad2Ae.Planetas.repository.PlanetaRepository;
import com.actividad2Ae.Planetas.services.PlanetaService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlanetaServiceImpl implements PlanetaService {

    @Autowired
    private PlanetaRepository planetasRepository;

    @Autowired
    private PlanetaMapper planetasMapper;


    @Override
    public PlanetaDto savePlanetas(PlanetaForm form) {

        Planeta planetas1 = new Planeta();
        planetas1.setName(form.getName());
        planetas1.setSize(form.getSize());

        Planeta save = planetasRepository.save(planetas1);

        return this.planetasMapper.map(save);
    }

    @Override
    public List<PlanetaDto> getAllPlanetasForms() {
        //obtengo la lista de planetas del repositorio
        List<Planeta> planetaList = this.planetasRepository.findAll();
        //despues se debe transformar a Dto
        List<PlanetaDto> dtoList = new ArrayList<>();
        //Creo una for que me recorra la lista origial
        for (Planeta planeObj : planetaList) {
            //Uso el mapper inyectado para trasformar cada entidad a Dto y se la paso a una variable
            PlanetaDto planetaDto = this.planetasMapper.map(planeObj);
            // paso el objeto a la lista
            dtoList.add(planetaDto);

        }
        return dtoList;
    }

    @Override
    public PlanetaDto updatePlanetas(Long id, PlanetaForm form) {

        //primero se averigua si esta el id en la bd

        Optional<Planeta> optionalPlanetas = this.planetasRepository.findById(id);

        //se evalua si el valor de la consulta anterior es vacio o no
        // si el id esta presente hacemos la logica

        if (optionalPlanetas.isPresent()) {

            //Logica de negocio parecida al save
            //Creamos una instancia de la entidad

            Planeta response = new Planeta();

            // le pasamos los datos a esa entidad, asi se remplazara en la tabla
            // esta vez es necesario darle el id que ya existe asi se actualizara de lo contrario crea una nueva entidad

            response.setId(optionalPlanetas.get().getId());
            //se le pasan los datos restantes que entraron asi como en el save
            response.setName(form.getName());
            response.setSize(form.getSize());
            //se persiste en bd y se retorna la entidad como en el save
            this.planetasRepository.save(response);


             return this.planetasMapper.map(response);


        }
        // si no esta presente retorna null
        return null;
    }

    @Override
    public String delate(Long id) {

        Optional<Planeta> optionalPlanetas = this.planetasRepository.findById(id);
        if (optionalPlanetas.isPresent()) {
            this.planetasRepository.deleteById(optionalPlanetas.get().getId());
            return "Id" + id + "fue eliminado";
        }
        return "id no eliminado";
    }
}


