package com.actividad2Ae.Planetas.services.impl;

import com.actividad2Ae.Planetas.data.dto.GalaxiaDto;
import com.actividad2Ae.Planetas.data.entity.Galaxia;
import com.actividad2Ae.Planetas.data.mapper.GalaxiaMapper;
import com.actividad2Ae.Planetas.data.payload.GalaxiaForm;
import com.actividad2Ae.Planetas.repository.GalaxiaRepository;
import com.actividad2Ae.Planetas.services.GalaxiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GalaxiaServicesImpl implements GalaxiaService {

    @Autowired
    private GalaxiaRepository galaxiaRepository;

    @Autowired
    private GalaxiaMapper galaxiaMapper;

    @Override
    public GalaxiaDto saveGalaxia(GalaxiaForm form) {

        Galaxia galaxia1 =  new Galaxia();
        galaxia1.setName(form.getName());
        galaxia1.setMovimiento(form.getMovimiento());

        Galaxia save = galaxiaRepository.save(galaxia1);
        return this.galaxiaMapper.map(save);
    }
}
