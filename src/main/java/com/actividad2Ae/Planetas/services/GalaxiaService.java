package com.actividad2Ae.Planetas.services;

import com.actividad2Ae.Planetas.data.dto.GalaxiaDto;
import com.actividad2Ae.Planetas.data.entity.Galaxia;
import com.actividad2Ae.Planetas.data.payload.GalaxiaForm;

import java.util.List;

public interface GalaxiaService {

    GalaxiaDto saveGalaxia(GalaxiaForm form);

}
