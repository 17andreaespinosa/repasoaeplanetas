package com.actividad2Ae.Planetas.services;

import com.actividad2Ae.Planetas.data.dto.PlanetaDto;
import com.actividad2Ae.Planetas.data.entity.Planeta;
import com.actividad2Ae.Planetas.data.payload.PlanetaForm;

import java.util.List;

public interface PlanetaService {

    PlanetaDto savePlanetas(PlanetaForm form);

    List<PlanetaDto> getAllPlanetasForms();

    PlanetaDto updatePlanetas(Long id, PlanetaForm form);

    String delate(Long id);
}
