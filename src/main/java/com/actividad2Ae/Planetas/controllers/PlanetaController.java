package com.actividad2Ae.Planetas.controllers;


import com.actividad2Ae.Planetas.data.dto.PlanetaDto;
import com.actividad2Ae.Planetas.data.entity.Planeta;
import com.actividad2Ae.Planetas.data.payload.PlanetaForm;
import com.actividad2Ae.Planetas.services.PlanetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/planetas")
public class PlanetaController {


    @Autowired
    private PlanetaService planetasService;

    @PostMapping("/save")
    public PlanetaDto savePlanetas(@RequestBody PlanetaForm datosPlanetas){
        return this.planetasService.savePlanetas(datosPlanetas );
    }
    @GetMapping("/all")
    public List<PlanetaDto>getAllPlanetas(){
        return this.planetasService.getAllPlanetasForms();
    }
    @PutMapping("/update/{id}")
    public PlanetaDto updatePlanteas(@PathVariable("id") Long id, @RequestBody PlanetaForm in){
        return this.planetasService.updatePlanetas(id, in);
    }

    @DeleteMapping("/delate/{id}")
    public String delatePlanetas(@PathVariable("id") Long id){
        return  this.planetasService.delate(id);
    }


}
