package com.actividad2Ae.Planetas.controllers;

import com.actividad2Ae.Planetas.data.dto.GalaxiaDto;
import com.actividad2Ae.Planetas.data.payload.GalaxiaForm;
import com.actividad2Ae.Planetas.services.GalaxiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/galaxia")
public class GalaxiaController {

    @Autowired
    private GalaxiaService galaxiaService;

    @PostMapping("/save")
    public GalaxiaDto saveGalaxia(@RequestBody GalaxiaForm datosGalaxia){
        return this.galaxiaService.saveGalaxia(datosGalaxia);

    }





}
