package com.actividad2Ae.Planetas.data.payload;

public class GalaxiaForm {

    private String name;
    private String movimiento;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }
}
