package com.actividad2Ae.Planetas.data.mapper;

import com.actividad2Ae.Planetas.data.dto.PlanetaDto;
import com.actividad2Ae.Planetas.data.entity.Planeta;
import org.springframework.stereotype.Component;

@Component
public class PlanetaMapper implements  IMapper<Planeta, PlanetaDto>{


    @Override
    public PlanetaDto map(Planeta in) {

        PlanetaDto planetasDto = new PlanetaDto();
        planetasDto.setName(in.getName());
        planetasDto.setSize(in.getSize());

        return planetasDto;
    }
}
