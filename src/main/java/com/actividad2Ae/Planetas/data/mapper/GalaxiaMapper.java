package com.actividad2Ae.Planetas.data.mapper;

import com.actividad2Ae.Planetas.data.dto.GalaxiaDto;
import com.actividad2Ae.Planetas.data.entity.Galaxia;
import org.springframework.stereotype.Component;

@Component
public class GalaxiaMapper implements IMapper<Galaxia, GalaxiaDto> {
    @Override
    public GalaxiaDto map(Galaxia in) {

        GalaxiaDto galaxiaDto = new GalaxiaDto();
        galaxiaDto.setName(in.getName());
        galaxiaDto.setMovimiento(in.getMovimiento());

        return galaxiaDto;
    }
}
