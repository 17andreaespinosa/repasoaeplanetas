package com.actividad2Ae.Planetas.data.entity;

import javax.persistence.*;

@Entity
@Table(name = "planetas")
public class Planeta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false, length = 50 )
    private String name;

    @Column(name =  "tamaño", nullable = false, length = 50)
    private String size;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getSize() {return size;}

    public void setSize(String size) {this.size = size;}
}
