package com.actividad2Ae.Planetas.repository;
import com.actividad2Ae.Planetas.data.entity.Planeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetaRepository extends JpaRepository<Planeta, Long>{


}
