package com.actividad2Ae.Planetas.repository;

import com.actividad2Ae.Planetas.data.entity.Galaxia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GalaxiaRepository extends JpaRepository<Galaxia,Long> {


}
